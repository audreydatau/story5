from django.shortcuts import render, redirect
from .models import add_schedule

def Homepage(request):
    return render(request, 'Homepage.html')

def About(request):
    return render(request, 'About.html')

def Experience(request):
    return render(request, 'Experience.html')

def Contact(request):
    return render(request, 'contact.html')

def Schedule(request):
    all_schedule=add_schedule.objects.all().order_by('time')
    context= {'all_schedule': all_schedule}
    if request.method == 'POST':
        schedule=add_schedule()
        schedule.activity_name= request.POST.get('activity_name')
        schedule.category= request.POST.get('category')
        schedule.place= request.POST.get('place')
        schedule.time= request.POST.get('time')
        schedule.save()
        return render(request, 'Schedule.html',context)
    else:
        return render(request, 'Schedule.html',context)
    

def Schedule_delete(request, num):
    obj = add_schedule.objects.all().order_by('time')
    obj_deleted = obj.get(id=num)
    obj_deleted.delete()
    return redirect('/Schedule/')

    
