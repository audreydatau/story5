from django.urls import path
from . import views

app_name = 'Homepage'

urlpatterns = [
    path('', views.Homepage, name='Homepage'),
    path('About/', views.About, name='About'),
    path('Experience/', views.Experience, name='Experience'),
    path('Contact/', views.Contact, name='Contact'),
    path('Schedule/', views.Schedule, name='Schedule'),
    path('Schedule/<int:num>', views.Schedule_delete, name='Schedule_delete'),
]
