from django.db import models
from django.utils import timezone
from datetime import datetime, date

class add_schedule(models.Model):
    activity_name = models.CharField(max_length=100)
    category = models.CharField(max_length=100)
    place = models.CharField(max_length=100)
    time = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.activity_name
